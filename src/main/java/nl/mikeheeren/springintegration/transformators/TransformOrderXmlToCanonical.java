package nl.mikeheeren.springintegration.transformators;

import nl.mikeheeren.springintegration.model.canonical.Order;
import nl.mikeheeren.springintegration.model.canonical.OrderLine;
import org.springframework.integration.transformer.AbstractPayloadTransformer;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TransformOrderXmlToCanonical extends AbstractPayloadTransformer<nl.mikeheeren.springintegration.model.xml.Order, Order> {

    @Override
    protected Order transformPayload(nl.mikeheeren.springintegration.model.xml.Order payload) {
        Order order = new Order();
        order.setId(payload.getId());
        order.setCustomerName(payload.getCustomer().getName());
        order.setOrderLines(payload.getLines().stream()
            .map(element -> {
                OrderLine orderLine = new OrderLine();
                orderLine.setArticle(element.getLocalName());
                NodeList childNodes = element.getChildNodes();
                for(int i = 0; i < childNodes.getLength(); i++) {
                    Node child = childNodes.item(i);
                    if("price".equalsIgnoreCase(child.getNodeName())) {
                        orderLine.setPrice(Double.parseDouble(child.getTextContent()));
                    } else if("amount".equalsIgnoreCase(child.getNodeName())) {
                        orderLine.setAmount(Integer.parseInt(child.getTextContent()));
                    }
                }
                return orderLine;
            })
            .toArray(OrderLine[]::new));
        return order;
    }

}

