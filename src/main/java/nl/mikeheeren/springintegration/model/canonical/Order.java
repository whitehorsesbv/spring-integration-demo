package nl.mikeheeren.springintegration.model.canonical;

import java.io.Serializable;
import java.util.Arrays;

public class Order implements Serializable {

    private String id;
    private String customerName;
    private OrderLine[] orderLines;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }

    public OrderLine[] getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(final OrderLine[] orderLines) {
        this.orderLines = orderLines;
    }

    /**
     * Read-only field to calculate the total price of all order lines.
     *
     * @return Total price of all order lines.
     */
    // Used to add totalPrice value when marshalling to XML.
    @SuppressWarnings("unused")
    public double getTotalPrice() {
        return Arrays.stream(getOrderLines())
                .mapToDouble(OrderLine::getTotalPrice)
                .sum();
    }

}
