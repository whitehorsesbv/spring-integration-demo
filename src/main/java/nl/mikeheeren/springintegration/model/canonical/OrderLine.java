package nl.mikeheeren.springintegration.model.canonical;

import java.io.Serializable;

public class OrderLine implements Serializable {

    private String article;
    private int amount;
    private double price;

    public String getArticle() {
        return article;
    }

    public void setArticle(final String article) {
        this.article = article;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(final int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    /**
     * Read-only field to calculate the total price of the order line. This field will not be marshalled to the JSON
     * structure.
     *
     * @return Total price of the order line.
     */
    double getTotalPrice() {
        return amount * price;
    }

}
