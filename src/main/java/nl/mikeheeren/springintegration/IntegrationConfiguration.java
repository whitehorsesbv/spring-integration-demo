package nl.mikeheeren.springintegration;

import nl.mikeheeren.springintegration.model.canonical.Order;
import nl.mikeheeren.springintegration.transformators.TransformOrderXmlToCanonical;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.http.HttpHeaders;
import org.springframework.integration.http.dsl.Http;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.integration.mongodb.outbound.MongoDbOutboundGateway;
import org.springframework.integration.mongodb.outbound.MongoDbStoringMessageHandler;
import org.springframework.integration.xml.transformer.UnmarshallingTransformer;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import java.io.File;
import java.util.HashMap;

@Configuration
public class IntegrationConfiguration {

    @Bean
    public IntegrationFlow fromFile(@Value("${order.file.location}") String orderFileLocation,
                                    JmsTemplate jmsTemplate) {
        return IntegrationFlows.from(Files.inboundAdapter(new File(orderFileLocation)),
                e -> e.poller(Pollers.fixedDelay(5000)))
                .filter("headers.file_name == 'orders.json'",
                        filterEndpointSpec -> filterEndpointSpec.discardFlow(df -> df
                                .log(LoggingHandler.Level.INFO, null, "'Invalid file received: ' + headers.file_name")
                                .nullChannel()))
                .transform(new JsonToObjectTransformer(Order[].class))
                .split()
                    .publishSubscribeChannel(s -> s
                        .subscribe(flow -> flow.handle(Jms.outboundAdapter(jmsTemplate).destination("queue:order"))))
                .aggregate()
                .log(LoggingHandler.Level.INFO, null, "'Finished dispatching orders to queue'")
                .get();
    }

    @Bean
    public IntegrationFlow fromAmqOrderQueue(JmsTemplate jmsTemplate,
                                             MongoTemplate mongoTemplate) {
        MongoDbStoringMessageHandler mongoDbStoringMessageHandler = new MongoDbStoringMessageHandler(mongoTemplate);
        mongoDbStoringMessageHandler.setCollectionNameExpression(new LiteralExpression("orders"));

        return IntegrationFlows.from(Jms.inboundAdapter(jmsTemplate).destination("queue:order"),
                e -> e.poller(Pollers.fixedDelay(5000)))
                .publishSubscribeChannel(s -> s
                        .subscribe(flow -> flow.handle(mongoDbStoringMessageHandler))
                        .subscribe(flow -> flow.log(LoggingHandler.Level.INFO, null, "'Successfully inserted into collection: ' + payload.id"))
                )
                .get();
    }

    @Bean
    public IntegrationFlow httpGetOrders(MongoTemplate mongoTemplate) {
        MongoDbOutboundGateway mongoDbOutboundGateway = new MongoDbOutboundGateway(mongoTemplate);
        mongoDbOutboundGateway.setEntityClass(Order.class);
        mongoDbOutboundGateway.setQueryExpression(new LiteralExpression("{}"));
        mongoDbOutboundGateway.setCollectionNameExpression(new LiteralExpression("orders"));

        return IntegrationFlows.from(Http.inboundGateway("/orders")
                .requestMapping(m -> m.methods(HttpMethod.GET)
                        .produces("application/json")))
                .handle(mongoDbOutboundGateway)
                .get();
    }

    @Bean
    public IntegrationFlow httpPostOrder(JmsTemplate jmsTemplate) {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(nl.mikeheeren.springintegration.model.xml.Order.class);

        return IntegrationFlows.from(Http.inboundGateway("/orders")
                .requestMapping(m -> m.methods(HttpMethod.POST)
                        .consumes("application/xml")
                        .produces("application/json")
                )
                .errorChannel("httpPostOrderErrorChannel.input"))
                .transform(new UnmarshallingTransformer(marshaller))
                .transform(new TransformOrderXmlToCanonical())
                .publishSubscribeChannel(s -> s
                        .subscribe(flow -> flow.handle(Jms.outboundAdapter(jmsTemplate).destination("queue:order")))
                        .subscribe(flow -> flow.transform(source -> {
                            HashMap<String, String> response = new HashMap<>();
                            response.put("message", "Successfully created order");
                            return response;
                        })))
                .get();
    }

    @Bean
    public IntegrationFlow httpPostOrderErrorChannel() {
        return f -> f
                .enrichHeaders(c -> c.header(HttpHeaders.STATUS_CODE, HttpStatus.INTERNAL_SERVER_ERROR))
                .transform(source -> {
                    HashMap<String, String> response = new HashMap<>();
                    response.put("message", "Failed to insert order");
                    return response;
                });
    }

}
